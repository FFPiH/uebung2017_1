import Aufgabe3

import Test.Framework.Providers.HUnit (testCase)
import Test.Framework.Runners.Console (defaultMain)

import Test.HUnit

import Lib

data TestProd = Tp1 | Tp2 | Tp3 deriving (Eq, Show)

productList :: ProductList Int TestProd ()
productList = insert (Article 0 Tp1 ())
            $ insert (Article 1 Tp2 ())
            $ insert (Article 2 Tp3 ())
            $ ListEnd

emptyScannerList :: ScannerList Int TestProd ()
emptyScannerList = AmountListEnd

scannerList1 = scan 0 productList emptyScannerList
scannerList2 = scan 9 productList emptyScannerList
scannerList3 = scannerList1 >>= scan 0 productList
scannerList4 = scannerList3 >>= scan 1 productList
scannerList5 = scannerList4 >>= scan 2 productList
scannerList6 = scannerList5 >>= scan 9 productList
scannerList7 = scannerList5 >>= scan 1 productList 
scannerList8 = scannerList7 >>= scan 0 productList
scannerList9 = scannerList8 >>= scan 9 productList


scan1 = testCase "Scan,     existing product, list size: 0                         "
      $ assertEqual "scan 1" (Just $ AmountAndElement 1 (Article 0 Tp1 ()) AmountListEnd) scannerList1

scan2 = testCase "Scan, not existing product, list size: 0                         "
      $ assertEqual "scan 2" Nothing scannerList2

scan3 = testCase "Scan,     existing product, list size: 1, product already in list"
      $ assertEqual "scan 3" (Just $ AmountAndElement 2 (Article 0 Tp1 ()) AmountListEnd) scannerList3

scan4 = testCase "Scan,     existing product, list size: 1, product not in list    "
      $ assertEqual "scan 4" (Just $ AmountAndElement 2 (Article 0 Tp1 ()) $ AmountAndElement 1 (Article 1 Tp2 ()) AmountListEnd) scannerList4

scan5 = testCase "Scan,     existing product, list size: 2, product not in list    "
      $ assertEqual "scan 5" (Just $ AmountAndElement 2 (Article 0 Tp1 ()) $ AmountAndElement 1 (Article 1 Tp2 ()) $ AmountAndElement 1 (Article 2 Tp3 ()) AmountListEnd) scannerList5

scan6 = testCase "Scan, not existing product, list size: 2                         "
      $ assertEqual "scan 3" Nothing scannerList6

scan7 = testCase "Scan,     existing product, list size: 3, product in list        "
      $ assertEqual "scan 7" (Just $ AmountAndElement 2 (Article 0 Tp1 ()) $ AmountAndElement 2 (Article 1 Tp2 ()) $ AmountAndElement 1 (Article 2 Tp3 ()) AmountListEnd) scannerList7

scan8 = testCase "Scan,     existing product, list size: 3, product in list        "
      $ assertEqual "scan 8" (Just $ AmountAndElement 3 (Article 0 Tp1 ()) $ AmountAndElement 2 (Article 1 Tp2 ()) $ AmountAndElement 1 (Article 2 Tp3 ()) AmountListEnd) scannerList8

scan9 = testCase "Scan, not existing product, list size: 3                         "
      $ assertEqual "scan 9" Nothing scannerList9


tests = [scan1, scan2, scan3, scan4, scan5, scan6, scan7, scan8, scan9]

main :: IO ()
main = defaultMain tests
