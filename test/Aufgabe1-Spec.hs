import Aufgabe1

import Test.Framework.Providers.HUnit (testCase)
import Test.Framework.Runners.Console (defaultMain)

import Test.HUnit

test1 = testCase "filter funktioniert" $ assertEqual "Vowel filter" "Hll Wrld!" result

testSentence2 = "Is really EVERY VowEL of this uSEleSS SentencE remOved?"
testResult2 = "s rlly VRY VwL f ths SlSS Sntnc rmvd?"
test2 = testCase "filter test" $ assertEqual testSentence2 testResult2 $ filterVowels testSentence2 

tests = [test1, test2]

main :: IO ()
main = defaultMain tests
