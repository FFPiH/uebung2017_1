import Aufgabe2

import Test.Framework.Providers.HUnit (testCase)
import Test.Framework.Runners.Console (defaultMain)

import Test.HUnit

import Lib

instance (Eq a, Eq b, Eq c) => Eq (Article a b c) where
  (Article a b c) == (Article a' b' c') = a == a' && b == b' && c == c'

data TestProd = Tp1 | Tp2 | Tp3 deriving (Eq, Show)

emptyProductList :: ProductList Int () ()
emptyProductList = ListEnd

productList :: ProductList Int TestProd ()
productList = insert (Article 0 Tp1 ())
            $ insert (Article 1 Tp2 ())
            $ insert (Article 2 Tp3 ())
            $ ListEnd

emptyFind = testCase "Suche in leerer Liste"
          $ assertEqual "empty list search" Nothing
          $ findArticle 0 emptyProductList

findNone = testCase "Suche nach nicht vorhandenem"
         $ assertEqual "find nothing" Nothing 
         $ findArticle 3 productList

findSome = testCase "Suche nach vorhandenem"
         $ assertEqual "find some" (Just $ Article 0 Tp1 ()) 
         $ findArticle 0 productList


tests = [emptyFind, findNone, findSome]

main :: IO ()
main = defaultMain tests
