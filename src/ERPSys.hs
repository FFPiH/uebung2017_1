module ERPSys
  ( ProductList
  , ScannerList
  , Article(Article)
  ) where

import List

type ProductList a b c = List (Article a b c) -- ^ List with Articles
type ScannerList a b c = AmountList Int (Article a b c) -- ^ AmountList with Articles

-- | generic Article isomorph to (,,)
data Article a b c = Article
 { _barcode :: a
 , _name    :: b
 , _price   :: c
 } deriving (Show)

