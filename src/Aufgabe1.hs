{-
        Aufgabe 1
        =========
-}
module Aufgabe1 where


{-      Gegeben ist der Datentyp Pred a, der eine Prüffunktion (Prädikat) repräsentiert, 
        die einen Wert vom Typ a zu einem Wert vom Typ Bool auswertet. 
        Beachten Sie, dass mit dem Wertekonstruktor Pred bereits eine Funktion 
        Pred :: (a -> Bool) -> Pred a 
        gegeben ist, mit der Sie ein Prädikat "einpacken" können.  
-}

newtype Pred a = Pred (a -> Bool)

{-      Schreiben Sie eine Funktion unPred, die das Prädikat "auspackt". 
-}

unPred :: Pred a -> (a -> Bool)
unPred = undefined

{-      Da Haskell-Funktionen grundsätzlich “gecurried” sind, bzw. der (->)-Operator  
        rechtsassoziativ ist, können Sie die Klammern hinten in der Signatur auch weglassen 
        und erhalten unPred :: Pred a -> a -> Bool, was man zugleich als “wende Pred a an, 
        wenn du ein a bekommst” lesen kann.
-}

{-      Definieren Sie nun eine Funktion isVowel, die prüft, ob ein Buchstabe ein Vokal ist.    
-}


isVowel :: Pred Char
isVowel = undefined


{-
        Schreiben Sie eine Funktion filterVowels, die alle Vorkommen von Vokalen aus
        einem String entfernt. Verwenden Sie hierfür das Prädikat isVowel. 
-}


filterVowels :: String -> String
filterVowels = undefined


result :: String
result = filterVowels "Hello World!"
