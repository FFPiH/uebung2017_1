{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE InstanceSigs #-}
{-# LANGUAGE FunctionalDependencies #-}
{-# LANGUAGE TemplateHaskell #-}

module List
  ( List(ListEnd, Element)
  , AmountList(AmountAndElement, AmountListEnd)
  , Filter
  , Searchable(..)
  , Consumable(..)
  , Insertable(..)
--  , amountOf
  ) where

import Control.Lens
import Data.Maybe (fromMaybe)

-- | isomorph to []
data List a = ListEnd | Element
  { _el        :: a 
  , _remaining :: (List a)
  } deriving (Show)

makeLenses (''List)

-- | counting list
data AmountList a b = AmountListEnd | AmountAndElement
  { _amount     :: a
  , _el'        :: b 
  , _remaining' :: (AmountList a b)
  } deriving (Show)

makeLenses (''AmountList)

type Filter a = a -> Bool

instance Monoid (List a) where
  mempty  = ListEnd
  mappend ListEnd = id
  mappend a = mappend $ a ^. remaining

instance Monoid (AmountList a b) where
  mempty  = AmountListEnd
  mappend AmountListEnd = id
  mappend a = mappend $ a ^. remaining'

instance Eq a => Eq (List a) where
  ListEnd == ListEnd = True
  a == b =  a^?el         == b^?el
         && a^?remaining  == b^?remaining

instance (Eq a, Eq b) => Eq (AmountList a b) where
  AmountListEnd == AmountListEnd = True
  a == b =  a^?amount     == b^?amount
         && a^?el'        == b^?el'
         && a^?remaining' == b^?remaining'

-- | find with return type determined by the structure b a
class Searchable a b c | b a -> c where
  findFirst :: Filter a -> b a -> Maybe c

-- | find first a in list matching the filter
instance Searchable a List a where
  findFirst :: Filter a -> List a -> Maybe a
  findFirst filter list = list^?el>>=(\el_-> if filter el_ 
                                             then Just el_ 
                                             else findFirst filter $ list^.remaining)

-- | find first a in list matching the filter returning (amount, element)
instance Searchable a (AmountList b) (b, a) where
  findFirst :: Filter a -> AmountList b a -> Maybe (b, a)
  findFirst filter list = list^?el' >>= \el_ -> if filter el_ 
                                                then Just (list^?!amount, el_) 
                                                else list^?remaining' >>= findFirst filter

class Consumable b a c | b a -> c  where
  consume :: (c -> d -> d) -> d -> b a -> d
  -- ^ special form of a fold determined by structure b a

-- | isomorph to a foldr
instance Consumable List a a where
  consume :: (a -> d -> d) -> d -> List a -> d
  consume _ a ListEnd = a
  consume f s (Element a as) = f a $ consume f s as

-- | foldr over (element, amount)
instance Consumable (AmountList b) a (a, b) where
  consume :: ((a, b) -> e -> e) -> e -> AmountList b a -> e
  consume _ a AmountListEnd = a
  consume f s (AmountAndElement b a as) = f (a,b) $ consume f s as


{--instance {-# OVERLAPPABLE #-} Consumable (AmountList b) a a where
  consume :: (a -> e -> e) -> e -> AmountList b a -> e
  consume _ a AmountListEnd = a
  consume f s (AmountAndElement b a as) = f a $ consume f s as --}

class Insertable a b where
  insert :: a -> b a -> b a
  -- ^ insert a to structure b

-- | RTFC!
instance Insertable a List where
  insert :: a -> List a -> List a
  insert = Element

-- | insert with amount count += 1
instance (Eq a, Num b) => Insertable a (AmountList b) where
  insert :: a -> AmountList b a -> AmountList b a
  insert a AmountListEnd = AmountAndElement 1 a AmountListEnd
  insert a b = case b^?el' & _Just %~ (==a) of
    Just True -> b & amount +~ 1
    _         -> b & remaining' %~ (insert a)

-- | get amount of a in list. returning mempty if not found
amountOf :: (Eq a, Monoid b) => a -> AmountList b a -> b
amountOf a b = fromMaybe mempty $ b ^. to (findFirst (==a)) & _Just %~ fst
--b ^. to findFirst (==a) %~ _1 ^. to fromMaybe mempty 
--  (Just (c,_)) -> c
--  Nothing      -> mempty

