Aufgabe 3
=========

> module Aufgabe3 where
>
> import Data.Maybe (fromMaybe)
>
> import List
>
> import ERPSys
>
> import Aufgabe2

Die Funktion scan soll, aus einer `ProductList` (2. Argument) anhand eines
Barcodes (1. Argument) einen Artikel finden und diesen dann in eine 
Scannerliste einfügen. Wurde kein Artiekl gefunden gibt `scan` `Nothing` zurück.
Um die eigentliche Funktion `scan` zu implementieren, empfiehlt es sich, zuerst
eine Instanz `Eq` für `Article` zu definieren.
Diese sollte anhand des Barcodes die Gleichheit eines Artikels bestimmen.

> scan :: (Eq a) => a -> ProductList a b c -> ScannerList a b c -> Maybe (ScannerList a b c)
>
> scan = undefined

> instance (Eq a) => Eq (Article a b c) where
>
>  (==) = undefined

> result = show $ scan 1 productCatalog
>        $ fromMaybe AmountListEnd $ scan 2 productCatalog
>        $ fromMaybe AmountListEnd $ scan 1 productCatalog AmountListEnd
