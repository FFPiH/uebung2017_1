-- Aufgabe 2
-- =========

module Aufgabe2 where

-- Machen Sie sich mit den Modulen

import List

import ERPSys

-- vertraut. Einige der Aufgaben diese Woche basieren auf diesen.

-- Hypothetisches Real-World Problem
-- ---------------------------------

-- Gegeben ein bereits existierendes Warenwirtschaftssystem (Modul ERPSys), welches
-- auf einer eigenen Datenstruktur (Modul List) basiert.

-- Die Firma, in der Sie arbeiten wird beauftragt, ein neues Kassensystem zu
-- entwickeln. Sie werden beauftragt, den Scanalogorithmus der Kasse zu
-- programmieren.



-- Dazu ist zuerst die Funktion `findArticle` zu entwickeln, die gegeben einen
-- Barcode und eine Produktliste einen Artikel findet.

findArticle :: (Eq a) => a -> ProductList a b c -> Maybe (Article a b c)

findArticle = undefined

productCatalog :: ProductList Int String Float
productCatalog = insert (Article 1 "Apfel" 1)
               $ insert (Article 2 "Birne" 0.5)
               $ insert (Article 3 "Banane" 1.5)
               $ insert (Article 4 "Tomate" 0.75)
                 ListEnd


result = show $ findArticle 2 productCatalog
