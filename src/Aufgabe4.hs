-- Aufgabe 4
-- =========

module Aufgabe4 where

import Lib
import Aufgabe2
import Aufgabe3
import Data.Maybe (fromMaybe)

-- Nun da eine `ScannerList` erstellt werden kann, wird eine Funktion, die einen Kassenbon erstellen kann, benötigt.
-- Dieser sollte angemessen formatiert sein.
-- Die Funktion `generateBill` soll diese Aufgabe übernehmen.

type Bill = String
 
generateBill :: (Show a, Show b, Show c) => ScannerList a b c -> Bill

generateBill = undefined


result = generateBill scannerList

-- Ein paar Hilfsfunktionen zum generieren einer `ScannerList` asu einer Liste
-- von Barcodes. /Glücklicherweise/ hat diese bereits ein Kollege von Ihnen entwickelt!
-- (Mit anderen Worten: Sie brauchen die unten stehenden Funktionen weder
-- anpassen, noch verstehen und auch nicht benutzen, nochmal Glück gehabt ;D )

scannerList :: ScannerList Int String Float
scannerList = fromMaybe AmountListEnd $ scanList [1,3,1,2,1,3]

scanList :: [Int] -> Maybe (ScannerList Int String Float)
scanList l = let help :: [Int] -> Maybe (ScannerList Int String Float) -> Maybe (ScannerList Int String Float)
                 help = flip $ foldl (\sl a -> sl >>= preparedScan a)
              in help l $ Just AmountListEnd

preparedScan :: Int -> ScannerList Int String Float -> Maybe (ScannerList Int String Float)

preparedScan = flip scan productCatalog

