Übungszettel 1
==============

Dieses Repository dient der ersten Übung des Moduls Fortgeschrittene
Funktionale Programmierung in Haskell. 
Wir werden sowohl in den Übungen als auch für das Projekt als Compiler den GHC
in Version >= 8 verwenden, sowie stack, ein Progamm zur Unterstützung der
Entwicklung von Haskell programmen.

Als Editor/IDE empfehlen wir nvim.


Stack
-----
> $ stack update

Neuste Paketliste laden


> $ stack setup

Läd den GHC und andere erforderliche Daten für ein das Projekt herunter



> $ stack build

Baut das Projekt



> $ stack test

Testet das Projekt



> $ stack haddock

Generiert die Dokumentation für das Projekt



> $ stack ghci

Compiliert das Projekt und startet den GHCi im Kontext des Projekts



> $ stack help

Try for yourself


Github
------

Wir verwenden Github statt des Lernraums/LernraumPLUS/moodle/etc.
Das bedeutet, dass ihr einen Github-Account braucht.

Das hoch und runterladen der Aufgaben / des Projekts erfolgt mit dem
Kommandozeilenprogramm git.

> $ git clone https://github.com/[..]

Klont ein git-repository in eine extra dafür neu angelegten Ordner

> $ git status

Zeigt den status des git-repository's an. Das bedeutet, welche Dateien
verändert wurden, und welche Änderungen "staged for commit" sind, dh. in einen
Commit einfließen werden.

> $ git commit -am "Aufgabe1.hs solved"

Speichert alle Änderungen, die staged sind, in einen Commit mit einer Commit
Message.

> $ git push

Pusht die Commits, dh. läd alle Commits hoch.

Falls ihr nvim verwendet, gibt es im Editor einige Plugins für eine direkte
Integration von git.

Github Classroom
----------------

Ihr werdet einen Link an eure Github-Mail erhalten, welcher euch Zugang einem
Assignment gibt.

Jedes Assignment erstellt ein neues privaten Repository mit der
Aufgabenstellung, in welches ihr eure Lösungen hochladen könnt.


Travis CI
---------

Travis ist ein continuous integration service. Immer wenn ihr eine Aufgabe
bearbeitet habt und diese auf Github pusht, läd Travis diesen push in eine
virtuelle Maschiene und baut und testet die neue Version.

Das bedeutet folgendes:
- Ihr bekommt direkt Feedback zu euren Lösungen
- Wir bekommen eine Mail wenn ein Build fehlschlägt/fehlerfrei durchläuft
- Ihr habt ein Konsolen-Log, in dem Ihr einezelne Zeilen makieren könnt und
  anschließend den Link an uns weiterleiten könnt um genaue Erklärungen
  bzw. Lösungshilfen zu Fehlern zu bekommen

Mit anderen Worten, ihr müsst nicht im Tutorium sitzen um effizient mit euren
Tutoren zu kommunizieren
